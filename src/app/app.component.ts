import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pizza-delivery';

  regularOrder = {
    small: 0,
    medium: 0,
    large: 0,
    extraLarge: 0,
    vegOptions: {
      tomatoes: 0,
      onions: 0,
      bellPepper: 0,
      mushrooms: 0,
      pineapple: 0
    },
    nonVegOptions: {
      sausage: 0,
      pepperoni: 0,
      bbqChicken: 0
    }
  };
  regularOrderTotal = 0;

  offerOrder = {
    offerOne: 0,
    offerTwo: 0,
    offerThree: 0,
    vegOptions: {
      tomatoes: 0,
      onions: 0,
      bellPepper: 0,
      mushrooms: 0,
      pineapple: 0
    },
    nonVegOptions: {
      sausage: 0,
      pepperoni: 0,
      bbqChicken: 0
    }
  };
  offerOrderTotal = 0;

  constructor() {}

  /*Regular Orders Start*/
  addOrderRegular(pizzaSize) {
    this.regularOrder[pizzaSize]++;
    this.calculateRegularOrderTotal();
  }

  removeOrderRegular(pizzaSize) {
    if (this.regularOrder[pizzaSize] === 0) { return; }
    this.regularOrder[pizzaSize]--;
    this.calculateRegularOrderTotal();
  }

  addOptionsRegular(optionType, option) {
    this.regularOrder[optionType][option]++;
    this.calculateRegularOrderTotal();
  }

  removeOptionsRegular(optionType, option) {
    if (this.regularOrder[optionType][option] === 0) { return; }
    this.regularOrder[optionType][option]--;
    this.calculateRegularOrderTotal();
  }

  calculateRegularOrderTotal() {
    this.regularOrderTotal = 0;
    this.regularOrderTotal += this.regularOrder.small * 5;
    this.regularOrderTotal += this.regularOrder.medium * 7;
    this.regularOrderTotal += this.regularOrder.large * 8;
    this.regularOrderTotal += this.regularOrder.extraLarge * 9;

    this.regularOrderTotal += this.regularOrder.vegOptions.tomatoes * 1;
    this.regularOrderTotal += this.regularOrder.vegOptions.onions * 0.50;
    this.regularOrderTotal += this.regularOrder.vegOptions.bellPepper * 1;
    this.regularOrderTotal += this.regularOrder.vegOptions.mushrooms * 1.20;
    this.regularOrderTotal += this.regularOrder.vegOptions.pineapple * 0.75;

    this.regularOrderTotal += this.regularOrder.nonVegOptions.sausage * 1;
    this.regularOrderTotal += this.regularOrder.nonVegOptions.pepperoni * 2;
    this.regularOrderTotal += this.regularOrder.nonVegOptions.bbqChicken * 3;
  }
  /*Regular Order End*/


  /*Offers Orders Start*/
  addOrderOffer(offer) {
    this.offerOrder[offer]++;
    this.calculateOfferOrderTotal();
  }

  removeOrderOffer(offer) {
    if (this.offerOrder[offer] === 0) { return; }
    this.offerOrder[offer]--;
    this.calculateOfferOrderTotal();
  }

  addOptionsOffer(optionType, option) {
    this.offerOrder[optionType][option]++;
    this.calculateOfferOrderTotal();
  }

  removeOptionsOffer(optionType, option) {
    if (this.offerOrder[optionType][option] === 0) { return; }
    this.offerOrder[optionType][option]--;
    this.calculateOfferOrderTotal();
  }

  calculateOfferOrderTotal() {
    let totalFreeToppings = 0;

    this.offerOrderTotal = 0;
    this.offerOrderTotal += this.offerOrder.offerOne * 5;
    this.offerOrderTotal += this.offerOrder.offerTwo * 9;
    this.offerOrderTotal += this.offerOrder.offerThree * 10;

    totalFreeToppings += this.offerOrder.offerOne * 2;
    totalFreeToppings += this.offerOrder.offerTwo * 8; // 4 toppings each
    totalFreeToppings += this.offerOrder.offerThree * 4;

    if (this.offerOrder.vegOptions.tomatoes <= totalFreeToppings) {
      totalFreeToppings -= this.offerOrder.vegOptions.tomatoes;
    } else {
      this.offerOrderTotal += (this.offerOrder.vegOptions.tomatoes - totalFreeToppings) * 1;
    }

    if (this.offerOrder.vegOptions.onions <= totalFreeToppings) {
      totalFreeToppings -= this.offerOrder.vegOptions.onions;
    } else {
      this.offerOrderTotal += (this.offerOrder.vegOptions.onions - totalFreeToppings) * 0.50;
    }

    if (this.offerOrder.vegOptions.bellPepper <= totalFreeToppings) {
      totalFreeToppings -= this.offerOrder.vegOptions.bellPepper;
    } else {
      this.offerOrderTotal += (this.offerOrder.vegOptions.bellPepper - totalFreeToppings) * 1;
    }

    if (this.offerOrder.vegOptions.mushrooms <= totalFreeToppings) {
      totalFreeToppings -= this.offerOrder.vegOptions.mushrooms;
    } else {
      this.offerOrderTotal += (this.offerOrder.vegOptions.mushrooms - totalFreeToppings) * 1.20;
    }

    if (this.offerOrder.vegOptions.pineapple <= totalFreeToppings) {
      totalFreeToppings -= this.offerOrder.vegOptions.pineapple;
    } else {
      this.offerOrderTotal += (this.offerOrder.vegOptions.pineapple - totalFreeToppings) * 0.75;
    }

    if (this.offerOrder.nonVegOptions.sausage <= totalFreeToppings) {
      totalFreeToppings -= this.offerOrder.nonVegOptions.sausage;
    } else {
      this.offerOrderTotal += (this.offerOrder.nonVegOptions.sausage - totalFreeToppings) * 1;
    }

    if (this.offerOrder.nonVegOptions.pepperoni <= totalFreeToppings) {
      totalFreeToppings -= this.offerOrder.nonVegOptions.pepperoni;
    } else {
      this.offerOrderTotal += (this.offerOrder.nonVegOptions.pepperoni - totalFreeToppings) * 2;
    }

    if (this.offerOrder.nonVegOptions.bbqChicken <= totalFreeToppings) {
      totalFreeToppings -= this.offerOrder.nonVegOptions.bbqChicken;
    } else {
      this.offerOrderTotal += (this.offerOrder.nonVegOptions.bbqChicken - totalFreeToppings) * 3;
    }
  }
  /*Offers Order End*/

}
